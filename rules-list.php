<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Rules List</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Rules List</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">	
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Manage</span>
									<span class="caption-helper">Rules List</span>
								</div>	
							</div>
							
							<div class="form-group well">
								<label>Choose project to set a rules</label>
								<div class="input-group">
								<span class="input-group-addon">
								<i class="fa fa-users"></i>
								</span>
								<select class="form-control select2_sample1" data-placeholder="Choose Project" id="respondentselector">
									<option value="1">Markplus Panel - V7 Batch 2</option>
									<option value="2">WOW BRand 2015 - CHM</option>
									<option value="3">Simbal</option>
									<option value="4">Auto Delloite - New</option>
								</select>
								</div>
							</div>


							<div class="portlet-body">
								<div class="row">
									<div class="col-sm-12">
										<div class="portlet light respondent" id="1">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Markplus Panel</span>
													<span class="caption-helper">V7 Batch 2</span>
												</div>
												<div class="actions">
													<a href="add-rules.php" class="btn btn-circle red-sunglo">
													<i class="fa fa-plus-circle"></i> Add Rules</a>	
												</div>
											</div>
											<div class="portlet-body">
													<table class="table table-striped table-bordered table-hover" id="sample_3">
													<thead>
													<tr>
														<th>No.</th>
														<th>Rules ID</th>
														<th>Survey Name</th>
														<th>Quota</th>
														<th>Quota Availability</th>
														<th>Respondent Availability</th>
														<th>Rules Condition</th>
														<th>Status</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>255 | 1</td>
														<td>Markplus Panel - V7 Batch 2</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													<tr class="odd gradeX">
														<td>2.</td>
														<td>255 | 1</td>
														<td>Markplus Panel - V7 Batch 2</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>
											</div>
										</div>

										<div class="portlet light respondent" id="2" style="display: none;">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> WOW BRand 2015</span>
													<span class="caption-helper">CHM</span>
												</div>
												<div class="actions">
													<a href="add-rules.php" class="btn btn-circle red-sunglo">
													<i class="fa fa-plus-circle"></i> Add Rules</a>	
												</div>
											</div>
											<div class="portlet-body">
													<table class="table table-striped table-bordered table-hover" id="sample_4">
													<thead>
													<tr>
														<th>No.</th>
														<th>Rules ID</th>
														<th>Survey Name</th>
														<th>Quota</th>
														<th>Quota Availability</th>
														<th>Respondent Availability</th>
														<th>Rules Condition</th>
														<th>Status</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>255 | 1</td>
														<td>WOW Brand 2015 - CHM</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													<tr class="odd gradeX">
														<td>2.</td>
														<td>255 | 1</td>
														<td>WOW Brand 2015 - CHM</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>
											</div>
										</div>

										<div class="portlet light respondent" id="3" style="display: none;">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Simbal</span>
													<span class="caption-helper"></span>
												</div>
												<div class="actions">
													<a href="add-rules.php" class="btn btn-circle red-sunglo">
													<i class="fa fa-plus-circle"></i> Add Rules</a>	
												</div>
											</div>
											<div class="portlet-body">
													<table class="table table-striped table-bordered table-hover" id="sample_5">
													<thead>
													<tr>
														<th>No.</th>
														<th>Rules ID</th>
														<th>Survey Name</th>
														<th>Quota</th>
														<th>Quota Availability</th>
														<th>Respondent Availability</th>
														<th>Rules Condition</th>
														<th>Status</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>255 | 1</td>
														<td>Simbal</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													<tr class="odd gradeX">
														<td>2.</td>
														<td>255 | 1</td>
														<td>Simbal</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>
											</div>
										</div>

										<div class="portlet light respondent" id="4" style="display: none;">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Auto Delloite</span>
													<span class="caption-helper">New</span>
												</div>
												<div class="actions">
													<a href="add-rules.php" class="btn btn-circle red-sunglo">
													<i class="fa fa-plus-circle"></i> Add Rules</a>	
												</div>
											</div>
											<div class="portlet-body">
													<table class="table table-striped table-bordered table-hover" id="sample_6">
													<thead>
													<tr>
														<th>No.</th>
														<th>Rules ID</th>
														<th>Survey Name</th>
														<th>Quota</th>
														<th>Quota Availability</th>
														<th>Respondent Availability</th>
														<th>Rules Condition</th>
														<th>Status</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>255 | 1</td>
														<td>Auto Delloite - New</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													<tr class="odd gradeX">
														<td>2.</td>
														<td>255 | 1</td>
														<td>Auto Delloite - New</td>
														<td>20</td>	
														<td>10</td>	
														<td>0</td>	
														<td>AND survey_id='739511'</td>
														<td class="text-center">
															<div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-animate">
																<div class="bootstrap-switch-container">
																	<input class="make-switch" checked="" data-on-color="danger" data-off-color="default" type="checkbox">
																</div>
															</div>
														</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>