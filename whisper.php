<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Whisper Monitoring </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Whisper Monitoring</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Whisper</span>
									<span class="caption-helper">Monitoring</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div class="form-group well">
										<label>Project</label>
										<div class="input-group">
										<span class="input-group-addon">
										<i class="fa fa-users"></i>
										</span>
										<select class="form-control select2_sample1" id="reportselector">
											<option value="1">Markplus Panel - V7 Batch 2</option>
											<option value="2">WOW BRand 2015 - CHM</option>
											<option value="3">Simbal</option>
											<option value="4">Auto Delloite - New</option>
										</select>
										</div>
									</div>

									<div id="filterButton">
										<button class="btn btn-default btn-md filter" data-filter="all"> View All</button>
										<button class="btn btn-success btn-md filter" data-filter=".online"> Online</button>
										<button class="btn btn-warning btn-md filter" data-filter=".idle"> Idle</button>
										<button class="btn grey btn-md filter" data-filter=".offline"> Offline</button>
									</div>

									</br>
								
									<div id="filterCaller">
										<div class="row">
											<div class="col-md-6 col-lg-3 mix offline">
												<div class="panel panel-default">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 01</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-default btn-sm" disabled><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar1.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm" disabled><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm" disabled><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix online">
												<div class="panel panel-success">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 02</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar2.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix online">
												<div class="panel panel-success">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 03</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar3.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix online">
												<div class="panel panel-success">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 04</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar4.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix idle">
												<div class="panel panel-warning">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 05</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-warning btn-sm" disabled><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar5.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm" disabled><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm" disabled><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix offline">
												<div class="panel panel-default">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 06</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-default btn-sm" disabled><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar6.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm" disabled><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm" disabled><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix idle">
												<div class="panel panel-warning">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 07</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-warning btn-sm" disabled><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar7.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm" disabled><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm" disabled><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix offline">
												<div class="panel panel-default">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 08</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-default btn-sm" disabled><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar8.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm" disabled><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm" disabled><i class="fa fa-minus-circle"></i> Abort</a> 
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix offline">
												<div class="panel panel-default">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 09</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-default btn-sm" disabled><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar9.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm" disabled><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm" disabled><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix online">
												<div class="panel panel-success">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 10</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar10.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix online">
												<div class="panel panel-success">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 11</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar11.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>

											<div class="col-md-6 col-lg-3 mix online">
												<div class="panel panel-success">
													<div class="panel-heading">
														<div class="row">
															<div class="col-xs-6">
																<h3 class="panel-title">CATI - 12</h3>
															</div>
															<div class="col-xs-6 text-right">
																<a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i></a>
															</div>
														</div>
													</div>
													<div class="panel-body text-center">
														 <img alt="" class="img-circle" src="img/avatar2.jpg"/></br></br>
														 <a href="#" class="btn btn-circle btn-success btn-sm"><i class="fa fa-phone"></i> Whisper</a>
														 <a href="#" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Abort</a>
													</div>
												</div>
											</div>
										</div>
									</div>

									<nav class="text-right">
									  <ul class="pagination pagination-sm">
									    <li>
									      <a href="#" aria-label="Previous">
									        <span aria-hidden="true">&laquo;</span>
									      </a>
									    </li>
									    <li class="active"><a href="#">1</a></li>
									    <li><a href="#">2</a></li>
									    <li><a href="#">3</a></li>
									    <li>
									      <a href="#" aria-label="Next">
									        <span aria-hidden="true">&raquo;</span>
									      </a>
									    </li>
									  </ul>
									</nav>		
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>