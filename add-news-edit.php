<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Add News </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Add News</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-md-9">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Add</span>
									<span class="caption-helper">News</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div data-always-visible="1" data-rail-visible1="1">
										<div class="form-group">
							              <label>Title <span class="small text-warning">(required)</span></label>
							              <div class="input-group">
							              <span class="input-group-addon">
							              <i class="fa fa-user"></i>
							              </span>
							              <input class="form-control" type="text" value="Lorem Ipsum Dolor Sit Amet"/>
							              </div>
							            </div>

							            <div class="form-group">
							              <label>Content</label>
							              <textarea class="ckeditor form-control" name="editor1" rows="5">
							              	Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
							                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							                when an unknown printer took a galley of type and scrambled it to make 
							                a type specimen book.
							              </textarea>
							            </div>
										
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="col-md-3">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Publish</span>
									<span class="caption-helper">News</span>
								</div>
								<div class="actions">
									<a href="#" class="btn btn-circle red-sunglo">
									<i class="fa fa-trash"></i> Delete</a>	
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<form class="form-horizontal sweet-form" role="form">
											<div class="form-group">
												<label class="control-label col-md-3">Status</label>
												<div class="col-md-9">
													<p class="form-control-static">-</p>
												</div>
											</div>
									</form>


									<div class="form-group">
										<div class="row">
											<div class="col-sm-6">
												<button type="button" class="btn default btn-block">Save as Draft</button>
											</div>
											<div class="col-sm-6">
												<a href="news-list.php" type="button" class="btn btn-success btn-block">Update</a>
											</div>
										</div>
									</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>