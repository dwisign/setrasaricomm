<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-auto-scroll="true" data-slide-speed="200">
					<li>
						<a href="index.php">
						<i class="icon-home"></i>
						<span class="title">Dashboard</span>
						<span class="selected"></span>
						</a>
					</li>
					<li class="start active ">
						<a href="javascript:;">
						<i class="fa fa-fax"></i>
						<span class="title">Dial</span>
						<span class="selected"></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="dial.php">
								<i class="fa fa-phone"></i>
								Dial</a>
							</li>
							<li>
								<a href="appointment-list.php">
								<i class="fa fa-list-ul "></i>
								Appointment List</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="call-history.php">
						<i class="fa fa-history"></i>
						<span class="title">Call History</span>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-2">
						<i class="fa fa-folder-open"></i>
						<span class="title">Complain Handling</span>
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="pull-right">
					<button type="button" class="btn btn-circle yellow"><b>Project :</b> WOW BRAND 2015 - CHM</button>
				</div>
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Dial </h3>
				<div class="page-bar">
					<div class="row">
						<div class="col-xs-4">
							<ul class="page-breadcrumb">
								<li>
									<i class="fa fa-home"></i>
									<a href="index.php">Home</a>
									<i class="fa fa-angle-right"></i>
								</li>
								<li>
									<a href="#">Dial</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-7">
							<div class="marquee"><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</a></div>
						</div>
						<div class="col-xs-1">
							<div class="page-toolbar">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
									NEWS
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>


			    <!-- Modal -->
				<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">CREATE APPOINTMENT</h4>
				      </div>
				      <div class="modal-body">
				      	<div class="alert alert-success alert-dismissable collapse" id="collapseExample-2">
			              Well done and everything looks OK. <a href="" class="alert-link">
			              Please check this one as well </a>
			            </div>

						<div class="form-group">
							<label>Name</label>
							<div class="input-group">
							<span class="input-group-addon">
							<i class="fa fa-user"></i>
							</span>
							<select class="form-control">
								<option value="1" selected="selected">Naoki Uehata</option>
								<option value="2">Kenji Sato</option>
								<option value="3">Desi Ottyari Ratna D</option>
								<option value="4">Akihiro Shimomura</option>
								<option value="5">Mokhtar</option>
								<option value="6">Laksmi Wulandari</option>
								<option value="7">Indriyasari, SE</option>
								<option value="8">Hadi Sumyartono</option>
							</select>
							</div>
						</div>

						<div class="form-group">
							<label>Date</label>
							<div class="input-group">
							<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
							</span>
							<input class="form-control form-control-inline date-picker" size="16" type="text" value=""/>
							</div>
						</div>

						<div class="form-group">
							<label>Time</label>
							<div class="input-group">
							<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
							</span>
							<input type="text" class="form-control timepicker timepicker-24">
							</div>
						</div>

						<div class="form-group">
							<label>Note</label>
							<textarea class="form-control" rows="3"></textarea>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-12">
									<button class="btn btn-success btn-block" data-toggle="collapse" href="#collapseExample-2">Save</button>
								</div>
							</div>
						</div>
				      </div>
				    </div>
				  </div>
				</div>


				<div class="row">
					<div class="col-md-3">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Dial</span>
									<span class="caption-helper"></span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div style="height: 305px;">
										<!-- BEGIN FORM-->
											<form class="form-horizontal sweet-form" role="form">

													<div class="form-group">
														<label class="control-label col-xs-5">Phone-1</br><a href="#" id="phone-1" data-type="text" data-placement="right" data-pk="1" data-original-title="Change phone-1">089653217475</a></label>
														<div class="col-xs-7">
															<div class="pull-right form-control-static">
																<a href="#dial-1" type="button" class="btn btn-circle btn-success"><i class="fa fa-phone"></i> Dial</a>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-5">Phone-2</br><a href="#" id="phone-2" data-type="text" data-placement="right" data-pk="2" data-original-title="Change phone-2">02157902338</a></label>
														<div class="col-xs-7">
															<div class="pull-right form-control-static">
																<a href="#dial-1" type="button" class="btn btn-circle btn-success"><i class="fa fa-phone"></i> Dial</a>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-5">Phone-3</br><span class="small"><i>-</i></span></label>
														<div class="col-xs-7">
															<div class="pull-right form-control-static">
																<a href="#dial-1" type="button" class="btn btn-circle btn-success"><i class="fa fa-phone"></i> Dial</a>
															</div>
														</div>
													</div>

													<hr>

													<div class="form-group">
														<div class="col-xs-12">
															<a href="#dial-1" type="button" class="btn btn-circle btn-success btn-block" id="keyboard"><i class="fa fa-phone"></i> Dial To Extension</a>
														</div>
													</div>

											</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Respondent</span>
									<span class="caption-helper">Detail</span>
								</div>
								<div class="actions">
									<a href="#" class="btn btn-circle red-sunglo ">
									<i class="fa fa-times-circle"></i> Belum Dihubungi</a>
									</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
										<!-- BEGIN FORM-->
											<form class="form-horizontal sweet-form" role="form">

													<div class="form-group">
														<label class="control-label col-md-3">Token</label>
														<div class="col-md-9">
															<p class="form-control-static">
																  CATJKTPELANGIR52
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Name</label>
														<div class="col-md-9">
															<p class="form-control-static">
																  Naoki Uehata
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">City</label>
														<div class="col-md-9">
															<p class="form-control-static">
																  Jakarta
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Time Zone</label>
														<div class="col-md-9">
															<p class="form-control-static">
																  -
															</p>
														</div>
													</div>	

													<div class="form-group">
														<label class="control-label col-md-3">Address</label>
														<div class="col-md-9">
															<p class="form-control-static">
																  2-18-12, Nishiochiai, Shinjuku-Ku, Tokyo, 161-8575 Japan
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Info-1</label>
														<div class="col-md-9">
															<p class="form-control-static">
																 List Responden SSS_GCA&AdvisoryClients_28Nov14  
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Info-2</label>
														<div class="col-md-9">
															<p class="form-control-static">
																 User 
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Info-3</label>
														<div class="col-md-9">
															<p class="form-control-static">
																PEMILIK PROYEK SEKTOR PUBLIK/GCA (GOVERNMENT CONTRACTING AGENCY) & ADVISORY CLIENTS   
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Info-4</label>
														<div class="col-md-9">
															<p class="form-control-static">
																Yachiyo Engineering Co., Ltd.   
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Info-5</label>
														<div class="col-md-9">
															<p class="form-control-static">
																 Chief, Social & Economic Infrastructure Dept.  
															</p>
														</div>
													</div>
											</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Progress</span>
									<span class="caption-helper">Fieldwork</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
										
										<!-- BEGIN FORM-->
											<form class="form-horizontal sweet-form" role="form">
													<div class="form-group">
														<label class="control-label col-md-3">Status</label>
														<div class="col-md-9">
															<select class="layout-option form-control" id="myModal">
																<option value="0">- Choose -</option>
																<option value="1">Sukses Interview</option>
																<option value="2">Reschedule/Jadwal Ulang</option>
																<option value="3">Menolak wawancara</option>
																<option value="#">Tidak Lolos Screening</option>
																<option value="#">Salah Sambung</option>
																<option value="#">Nomor Tidak Terpasang</option>
																<option value="#">Tidak Ada Nada</option>
																<option value="#">Tidak Ada Jawaban</option>
																<option value="#">Tidak Aktif</option>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Note</label>
														<div class="col-md-9">
															<textarea class="form-control" rows="8"></textarea>
														</div>
													</div>
											</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="clearfix">
				</div>

				<div class="row" id="dial-1">
					<div class="col-sm-3">
						<div class="portlet light tasks-widget">
							<div class="portlet-body">
								<div class="task-content">
									<div class="alert alert-warning text-center">
										<h4>00 : 00 : 00</h4>
									</div>
									<textarea class="form-control" rows="5">I'm ready to call...</textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-9">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Quesioner</span>
									<span class="caption-helper">WOW BRAND 2015 - CHM</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="clearfix">
				</div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>