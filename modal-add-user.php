<!-- Modal -->
<div class="modal fade" id="myModal-7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD USER</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success alert-dismissable collapse" id="alert-2">
              Well done and everything looks OK. <a href="" class="alert-link">
              Please check this one as well </a>
            </div>


            <div class="form-group">
              <label class="control-label">Select Project <span class="small text-warning">(You can choose more than one project)</span></label>
              <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-file"></i>
                </span>
                <select class="form-control select2_sample1" data-placeholder="Select Project" tabindex="1" multiple >
                  <option>WOW Brand 2015 - CHM</option>
                  <option>Simbal</option>
                  <option>Markplus Panel - V7 batch 2</option>
                  <option>Auto Delloite New</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label>Extention <span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-share"></i>
              </span>
              <input class="form-control" type="text" placeholder="Extention"/>
              </div>
            </div>

            <div class="form-group">
              <label>Extention Password <span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-lock"></i>
              </span>
              <input class="form-control" type="text" placeholder="Ext. Password"/>
              </div>
            </div>

            <div class="form-group">
              <label>Full Name <span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-user"></i>
              </span>
              <input class="form-control" type="text" placeholder="Full Name"/>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label">User Category <span class="small text-warning">(required)</span></label>
              <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-list-ol"></i>
                </span>
                <select class="form-control select2_sample1" data-placeholder="User Category" >
                  <option>Superviser</option>
                  <option>Interviewer</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label>User Name <span class="small text-warning">(required - user "token" will be using this username)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-user"></i>
              </span>
              <input class="form-control" type="text" placeholder="User Name"/>
              </div>
            </div>

            <div class="form-group">
              <label>Password<span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-lock"></i>
              </span>
              <input class="form-control" type="password" placeholder="Password"/>
              </div>
            </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="collapse" href="#alert-2">Save</button>
      </div>
    </div>
  </div>
</div>