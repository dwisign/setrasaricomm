<!-- Modal -->
<div class="modal fade" id="myModal-8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD APPOINTMENT</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success alert-dismissable collapse" id="alert-2">
              Well done and everything looks OK. <a href="" class="alert-link">
              Please check this one as well </a>
            </div>

            <div class="form-group">
              <label>Project</label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-file"></i>
              </span>
              <select class="form-control select2_sample1" data-placeholder="Project" >
                  <option value="1">Markplus Panel - V7 Batch 2</option>
                  <option value="2">WOW BRand 2015 - CHM</option>
                  <option value="3">Simbal</option>
                  <option value="4">Auto Delloite - New</option>
              </select>
              </div>
            </div>

            <div class="form-group">
              <label>User</label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-user"></i>
              </span>
              <select class="form-control select2_sample1" data-placeholder="User" >
                  <option value="1">CATI-01</option>
                  <option value="2">CATI-02</option>
                  <option value="3">CATI-03</option>
                  <option value="4">CATI-04</option>
              </select>
              </div>
            </div>

            <div class="form-group">
              <label>Respondent</label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-users"></i>
              </span>
              <select class="form-control select2_sample1" data-placeholder="Respondent" >
                  <option value="0">-- select respondent --</option>
                  <option value="7|TAY LIAN CHEW">TAY LIAN CHEW | List Responden SSS_GCA&amp;AdvisoryClients_28Nov14</option>
                  <option value="11|LAKSMI WULANDARI">LAKSMI WULANDARI | LIST RESPONDENT STAKEHOLDERS SATISFACTION SURVEY</option>
                  <option value="4|IR. TRI RISMAHARINI, MT">IR. TRI RISMAHARINI, MT | List Responden SSS_GCA&amp;AdvisoryClients_28Nov14</option>
                  <option value="8|INDRIYASARI, SE">INDRIYASARI, SE | List Responden SSS_GCA&amp;AdvisoryClients_28Nov14</option>
                  <option value="2|HADI SUMYARTONO">HADI SUMYARTONO | List Responden SSS_Debitur</option>
                  <option value="6|AKIHIRO SHIMOMURA">AKIHIRO SHIMOMURA | List Responden SSS_GCA&amp;AdvisoryClients_28Nov14</option>
              </select>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Date</label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                  </span>
                  <input class="form-control form-control-inline date-picker" size="16" type="text" value=""/>
                  </div>
                </div>

                <div class="col-sm-6">
                  <label>Time</label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                  </span>
                  <input type="text" class="form-control timepicker timepicker-24">
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control" rows="3"></textarea>
            </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="collapse" href="#alert-2">Add Appointment</button>
      </div>
    </div>
  </div>
</div>