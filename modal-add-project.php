<!-- Modal -->
<div class="modal fade" id="myModal-3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD PROJECT</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success alert-dismissable collapse" id="alert-2">
              Well done and everything looks OK. <a href="" class="alert-link">
              Please check this one as well </a>
            </div>

            <div class="form-group">
              <label>Project Extention</label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-share-alt"></i>
              </span>
              <select class="form-control select2_sample1" data-placeholder="Project Extention" >
                <option value="1" selected="selected">0</option>
                <option value="2">1</option>
                <option value="3">2</option>
                <option value="3">3</option>
                <option value="3">4</option>
              </select>
              </div>
            </div>

            <div class="form-group">
              <label>Name <span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-user"></i>
              </span>
              <input class="form-control" type="text" placeholder="Name"/>
              </div>
            </div>

            <div class="form-group">
              <label>Survey id <span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-check-circle-o"></i>
              </span>
              <input class="form-control" type="text" placeholder="Survey ID"/>
              </div>
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control" rows="3"></textarea>
            </div>

            <div class="form-group">
              <label>Target Day <span class="small text-warning">(required)</span></label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-calendar"></i>
              </span>
              <input class="form-control" type="text" placeholder="Target Day"/>
              </div>
            </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="collapse" href="#alert-2">Add Project</button>
      </div>
    </div>
  </div>
</div>