<!-- Modal -->
<div class="modal fade" id="myModal-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD RESPONDENT</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success alert-dismissable collapse" id="alert-1">
              Well done and everything looks OK. <a href="" class="alert-link">
              Please check this one as well </a>
            </div>

            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                    <label>Project <span class="small text-warning">(required)</span></label>
                    <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fa fa-share-alt"></i>
                    </span>
                    <select class="form-control select2_sample1" data-placeholder="Choose Project">
                      <option value="1" selected="selected">- Select Project -</option>
                      <option value="1">Markplus Panel - V7 Batch 2</option>
                      <option value="2">WOW BRand 2015 - CHM</option>
                      <option value="3">Simbal</option>
                      <option value="4">Auto Delloite - New</option>
                    </select>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Name <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-user"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Name"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Last Name <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-user"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Last Name"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>City <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-map-marker"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="City"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3"></textarea>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label>Phone-1 <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Phone-1"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label>Phone-2 <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Phone-2"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label>Phone-3 <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Phone-3"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Info-1 <span class="small text-warning">(required)</span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-info"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Info-1"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Info-2 <span class="small text-warning"></span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-info"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Info-2"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Info-3 <span class="small text-warning"></span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-info"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Info-3"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Info-4 <span class="small text-warning"></span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-info"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Info-4"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Info-5 <span class="small text-warning"></span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-info"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Info-5"/>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Time Zone <span class="small text-warning"></span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                  </span>
                  <select class="form-control select2_sample1" data-placeholder="Choose Time Zone">
                    <option value="1" selected="selected">- Select Time Zone -</option>
                    <option value="1">WIB</option>
                    <option value="2">WIT</option>
                    <option value="3">WITA</option>
                  </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Token <span class="small text-warning"></span></label>
                  <div class="input-group">
                  <span class="input-group-addon">
                  <i class="fa fa-lock"></i>
                  </span>
                  <input class="form-control" type="text" placeholder="Token"/>
                  </div>
                </div>
              </div>
            </div>



      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="collapse" href="#alert-1">Add Respondent</button>
      </div>
    </div>
  </div>
</div>