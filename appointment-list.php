<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-auto-scroll="true" data-slide-speed="200">
					<li>
						<a href="index.php">
						<i class="icon-home"></i>
						<span class="title">Dashboard</span>
						</a>
					</li>
					<li class="start active">
						<a href="javascript:;">
						<i class="fa fa-fax"></i>
						<span class="title">Dial</span>
						<span class="selected"></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="dial.php">
								<i class="fa fa-phone"></i>
								Dial</a>
							</li>
							<li>
								<a href="appointment-list.php">
								<i class="fa fa-list-ul "></i>
								Appointment List</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="call-history.php">
						<i class="fa fa-history"></i>
						<span class="title">Call History</span>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-2">
						<i class="fa fa-folder-open"></i>
						<span class="title">Complain Handling</span>
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="pull-right">
					<button type="button" class="btn btn-circle yellow"><b>Project :</b> WOW BRAND 2015 - CHM</button>
				</div>
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Appointment List </h3>
				<div class="page-bar">
					<div class="row">
						<div class="col-xs-4">
							<ul class="page-breadcrumb">
								<li>
									<i class="fa fa-home"></i>
									<a href="index.php">Home</a>
									<i class="fa fa-angle-right"></i>
								</li>
								<li>
									<a href="#">Appointment List</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-7">
							<div class="marquee"><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</a></div>
						</div>
						<div class="col-xs-1">
							<div class="page-toolbar">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
									NEWS
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				

				<div class="clearfix">
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Appointment</span>
									<span class="caption-helper">List</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
										

										<!-- BEGIN EXAMPLE TABLE PORTLET-->
										<table class="table table-striped table-bordered table-hover" id="sample_3">
										<thead>
										<tr>
											<th>No.</th>
											<th>Respondent Name</th>
											<th>Call Time</th>
											<th>Token</th>
											<th>Phone Number</th>
											<th>Description</th>
											<th>Action</th>
										</tr>
										</thead>
										<tbody>
										<tr class="odd gradeX">
											<td>1.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>2.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>3.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>4.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>5.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>6.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>7.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>8.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>

										<tr class="odd gradeX">
											<td>9.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>2015-01-07 11:56:35</td>
											<td>CATJKTPELANGIR50</td>	
											<td>
												021345678
											</td>
											<td>-</td>	
											<td class="text-center"><a href="#"><i class="fa fa-times-circle"></i></a></td>
										</tr>
										
										</tbody>
										</table>
										</br>

										<!-- END EXAMPLE TABLE PORTLET-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>