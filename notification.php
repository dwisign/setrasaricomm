<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-auto-scroll="true" data-slide-speed="200">
					<li>
						<a href="index.php">
						<i class="icon-home"></i>
						<span class="title">Dashboard</span>
						</a>
					</li>
					<li class="start active">
						<a href="javascript:;">
						<i class="fa fa-fax"></i>
						<span class="title">Dial</span>
						<span class="selected"></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="dial.php">
								<i class="fa fa-phone"></i>
								Dial</a>
							</li>
							<li>
								<a href="appointment-list.php">
								<i class="fa fa-list-ul "></i>
								Appointment List</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="call-history.php">
						<i class="fa fa-history"></i>
						<span class="title">Call History</span>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-2">
						<i class="fa fa-folder-open"></i>
						<span class="title">Complain Handling</span>
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="pull-right">
					<button type="button" class="btn btn-circle yellow"><b>Project :</b> WOW BRAND 2015 - CHM</button>
				</div>
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Notification </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.php">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Notification</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				

				<div class="clearfix">
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Notification</span>
									<span class="caption-helper">List</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
										<div class="list-group">
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a>
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a>
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a>
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a>
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a>
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a> 
										  <a href="dial.php" class="list-group-item">
										  	<div class="row">
										  		<div class="col-sm-9">
										  			<h5 class="list-group-item-heading"><b>NAOKI UEHATA</b></h5>
										    		<p class="list-group-item-text">30/04/2015 | 06.32.00</p>
										  		</div>
										  		<div class="col-sm-3 text-right">
													<i class="fa fa-phone"></i>
										  		</div>
										  	</div>
										  </a>   
									</div>

									<nav>
									  <ul class="pagination pagination-sm">
									    <li>
									      <a href="#" aria-label="Previous">
									        <span aria-hidden="true">&laquo;</span>
									      </a>
									    </li>
									    <li><a href="#">1</a></li>
									    <li><a href="#">2</a></li>
									    <li><a href="#">3</a></li>
									    <li><a href="#">4</a></li>
									    <li><a href="#">5</a></li>
									    <li>
									      <a href="#" aria-label="Next">
									        <span aria-hidden="true">&raquo;</span>
									      </a>
									    </li>
									  </ul>
									</nav>


								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>