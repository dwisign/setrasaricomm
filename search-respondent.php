<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Search Respondent</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Search Respondent</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">	
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Search</span>
									<span class="caption-helper">Respondent</span>
								</div>
								<div class="actions">
									<a href="#" class="btn btn-circle red-sunglo" data-toggle="modal" data-target="#myModal-4">
									<i class="fa fa-plus-circle"></i> Add Respondent</a>
									<a href="#" class="btn btn-circle btn-success" data-toggle="modal" data-target="#myModal-5">
									<i class="fa fa-download"></i> Import Respondent</a>	
								</div>	
							</div>
							
							<div class="form-group well">
								<label>Respondent List</label>
								<div class="input-group">
								<span class="input-group-addon">
								<i class="fa fa-users"></i>
								</span>
								<select class="form-control select2_sample1" data-placeholder="User Category" id="respondentselector">
									<option value="1">Markplus Panel - V7 Batch 2</option>
									<option value="2">WOW BRand 2015 - CHM</option>
									<option value="3">Simbal</option>
									<option value="4">Auto Delloite - New</option>
								</select>
								</div>
							</div>


							<div class="portlet-body">
								<div class="row">
									<div class="col-sm-12">
										<div class="portlet light respondent" id="1">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Markplus Panel</span>
													<span class="caption-helper">V7 Batch 2</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label>Interviewer</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Interviewer">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Call Status</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-phone"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Call Status">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Sukses Interview</option>
																	<option value="2">Menolak Wawancara</option>
																	<option value="3">Tidak Lolos Screening</option>
																	<option value="3">Salah Sambung</option>
																	<option value="3">Nomor Tidak Terpasang</option>
																	<option value="3">Tidak Ada Nada</option>
																	<option value="3">Tidak Ada Jawaban</option>
																	<option value="3">Tidak Aktif</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Name</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-user"></i>
																</span>
																<input type="text" class="form-control" placeholder="Name">
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>


													<table class="table table-striped table-bordered table-hover" id="sample_3">
													<thead>
													<tr>
														<th>No.</th>
														<th>Survey ID</th>
														<th>Project Name</th>
														<th>Name</th>
														<th>Token</th>
														<th>Call Status</th>
														<th>Call By</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>2532532</td>
														<td>Markplus Panel - V7 Batch 2</td>
														<td>-</td>	
														<td>-</td>	
														<td>Menolak Wawancara</td>	
														<td>-</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>


											</div>
										</div>

										<div class="portlet light respondent" id="2" style="display: none;">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> WOW Brand 2015</span>
													<span class="caption-helper">CHM</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label>Interviewer</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Interviewer">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Call Status</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-phone"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Call Status">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Sukses Interview</option>
																	<option value="2">Menolak Wawancara</option>
																	<option value="3">Tidak Lolos Screening</option>
																	<option value="3">Salah Sambung</option>
																	<option value="3">Nomor Tidak Terpasang</option>
																	<option value="3">Tidak Ada Nada</option>
																	<option value="3">Tidak Ada Jawaban</option>
																	<option value="3">Tidak Aktif</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Name</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-user"></i>
																</span>
																<input type="text" class="form-control" placeholder="Name">
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>


													<table class="table table-striped table-bordered table-hover" id="sample_4">
													<thead>
													<tr>
														<th>No.</th>
														<th>Survey ID</th>
														<th>Project Name</th>
														<th>Name</th>
														<th>Token</th>
														<th>Call Status</th>
														<th>Call By</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>2532532</td>
														<td>WOW Brand 2015 - CHM</td>
														<td>-</td>	
														<td>-</td>	
														<td>Menolak Wawancara</td>	
														<td>-</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>
	

											</div>
										</div>

										<div class="portlet light respondent" id="3" style="display: none;">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Simbal</span>
													<span class="caption-helper"></span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label>Interviewer</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Interviewer">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Call Status</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-phone"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Call Status">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Sukses Interview</option>
																	<option value="2">Menolak Wawancara</option>
																	<option value="3">Tidak Lolos Screening</option>
																	<option value="3">Salah Sambung</option>
																	<option value="3">Nomor Tidak Terpasang</option>
																	<option value="3">Tidak Ada Nada</option>
																	<option value="3">Tidak Ada Jawaban</option>
																	<option value="3">Tidak Aktif</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Name</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-user"></i>
																</span>
																<input type="text" class="form-control" placeholder="Name">
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>


													<table class="table table-striped table-bordered table-hover" id="sample_5">
													<thead>
													<tr>
														<th>No.</th>
														<th>Survey ID</th>
														<th>Project Name</th>
														<th>Name</th>
														<th>Token</th>
														<th>Call Status</th>
														<th>Call By</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>2532532</td>
														<td>Simbal</td>
														<td>-</td>	
														<td>-</td>	
														<td>Menolak Wawancara</td>	
														<td>-</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>


											</div>
										</div>

										<div class="portlet light respondent" id="4" style="display: none;">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Auto Delloite</span>
													<span class="caption-helper">New</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label>Interviewer</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Interviewer">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Call Status</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-phone"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Call Status">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Sukses Interview</option>
																	<option value="2">Menolak Wawancara</option>
																	<option value="3">Tidak Lolos Screening</option>
																	<option value="3">Salah Sambung</option>
																	<option value="3">Nomor Tidak Terpasang</option>
																	<option value="3">Tidak Ada Nada</option>
																	<option value="3">Tidak Ada Jawaban</option>
																	<option value="3">Tidak Aktif</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Name</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-user"></i>
																</span>
																<input type="text" class="form-control" placeholder="Name">
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>


													<table class="table table-striped table-bordered table-hover" id="sample_6">
													<thead>
													<tr>
														<th>No.</th>
														<th>Survey ID</th>
														<th>Project Name</th>
														<th>Name</th>
														<th>Token</th>
														<th>Call Status</th>
														<th>Call By</th>
														<th>Modify</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>2532532</td>
														<td>Auto Delloite - New</td>
														<td>-</td>	
														<td>-</td>	
														<td>Menolak Wawancara</td>	
														<td>-</td>
														<td class="text-center">
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
															<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
														</td>
													</tr>
													
													</tbody>
													</table>


											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>