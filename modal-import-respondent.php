<!-- Modal -->
<div class="modal fade" id="myModal-5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">IMPORT RESPONDENT</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success alert-dismissable collapse" id="alert-5">
              Well done and everything looks OK. <a href="" class="alert-link">
              Please check this one as well </a>
            </div>

            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                    <label>Project <span class="small text-warning">(required)</span></label>
                    <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fa fa-share-alt"></i>
                    </span>
                    <select class="form-control select2_sample1" data-placeholder="Choose Project">
                      <option value="1" selected="selected">- Select Project -</option>
                      <option value="1">Markplus Panel - V7 Batch 2</option>
                      <option value="2">WOW BRand 2015 - CHM</option>
                      <option value="3">Simbal</option>
                      <option value="4">Auto Delloite - New</option>
                    </select>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Attachments</label>
                  <div class="input-group">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="input-group input-large">
                      <div class="form-control uneditable-input span3" data-trigger="fileinput">
                        <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"></span>
                      </div>
                      <span class="input-group-addon btn default btn-file">
                      <span class="fileinput-new">
                      Select file </span>
                      <span class="fileinput-exists">
                      Change </span>
                      <input name="..." value="" type="hidden"><input name="" type="file">
                      </span>
                      <a href="#" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">
                      Remove </a>
                    </div>
                   </div>
                  </div>
                </div>
              </div>
            </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="collapse" href="#alert-5">Import Respondent</button>
      </div>
    </div>
  </div>
</div>