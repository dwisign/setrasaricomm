
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner container-fluid">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="index.php">
				<img class="logo-default" src="img/logo-mpi.png" width="110"> 
				</a>
				<div class="menu-toggler sidebar-toggler">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN PAGE TOP -->
			<div class="page-top">
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<!-- BEGIN NOTIFICATION DROPDOWN -->
						<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="icon-bell"></i>
							<span class="badge badge-danger">
							5 </span>
							</a>
							<ul class="dropdown-menu">
								<li>
									<p>
										 You have 5 new notifications
									</p>
								</li>
								<li>
									<ul class="dropdown-menu-list scroller" style="height: 250px;">
										<li>
											<a href="dial.php">
											<div class="row">
												<div class="col-xs-2">
													<span class="label label-sm label-icon label-success">
													<i class="fa fa-phone"></i>
													</span>
												</div>
												<div class="col-xs-10">
													Naoki Uehata </br><span class="time">
													30/04/2015, 06:32:00</span>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="dial.php">
											<div class="row">
												<div class="col-xs-2">
													<span class="label label-sm label-icon label-success">
													<i class="fa fa-phone"></i>
													</span>
												</div>
												<div class="col-xs-10">
													Naoki Uehata </br><span class="time">
													30/04/2015, 06:32:00</span>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="dial.php">
											<div class="row">
												<div class="col-xs-2">
													<span class="label label-sm label-icon label-success">
													<i class="fa fa-phone"></i>
													</span>
												</div>
												<div class="col-xs-10">
													Naoki Uehata </br><span class="time">
													30/04/2015, 06:32:00</span>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="dial.php">
											<div class="row">
												<div class="col-xs-2">
													<span class="label label-sm label-icon label-success">
													<i class="fa fa-phone"></i>
													</span>
												</div>
												<div class="col-xs-10">
													Naoki Uehata </br><span class="time">
													30/04/2015, 06:32:00</span>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="dial.php">
											<div class="row">
												<div class="col-xs-2">
													<span class="label label-sm label-icon label-success">
													<i class="fa fa-phone"></i>
													</span>
												</div>
												<div class="col-xs-10">
													Naoki Uehata </br><span class="time">
													30/04/2015, 06:32:00</span>
												</div>
											</div>
											</a>
										</li>
									</ul>
								</li>
								<li class="external">
									<a href="notification.php">
									See all notifications <i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>
						<!-- END NOTIFICATION DROPDOWN -->

						<!-- BEGIN QUICK SIDEBAR TOGGLER -->
						<li class="dropdown dropdown-quick-sidebar-toggler hide">
							<a href="javascript:;" class="dropdown-toggle">
							<i class="icon-logout"></i>
							</a>
						</li>
						<!-- END QUICK SIDEBAR TOGGLER -->
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown dropdown-user">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="img/avatar2.jpg"/>
							<span class="username username-hide-on-mobile">
							Welcome | CATI-09 </span>
							<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="profile.php">
									<i class="icon-user"></i> My Profile </a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="login.php">
									<i class="icon-key"></i> Log Out </a>
								</li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
				</div>
				<!-- END TOP NAVIGATION MENU -->
			</div>
			<!-- END PAGE TOP -->
		</div>
		<!-- END HEADER INNER -->
	</div>
