<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery.pulsate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.zh-CN.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.mockjax.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
<script type="text/javascript" src="assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="js/metronic.js" type="text/javascript"></script>
<script src="js/layout.js" type="text/javascript"></script>
<script src="js/demo.js" type="text/javascript"></script>
<script src="js/index.js" type="text/javascript"></script>
<script src="js/tasks.js" type="text/javascript"></script>
<script src="js/table-managed.js"></script>
<script src="js/components-pickers.js"></script>
<script src="js/form-editable.js"></script>
<script src="js/login-soft.js" type="text/javascript"></script>
<script src="js/form-samples.js"></script>
<script src="js/jquery.mixitup.js"></script>
<script src="js/jquery.marquee.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- keyboard virtual -->
<script src="js/jquery.keyboard.js" type="text/javascript"></script>
<script src="js/jquery.keyboard.extension-all.js" type="text/javascript"></script>
<!-- highchart -->
<script src="js/highcharts.js" type="text/javascript"></script>
<script src="js/exporting.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo features 
   Index.init();   
   Index.initDashboardDaterange();
   Index.initCalendar(); 
   Index.initChat();
   Index.initIntro();
   Tasks.initDashboardWidget();
   TableManaged.init();
   ComponentsPickers.init();
   FormEditable.init();
   FormSamples.init();
});
</script>

<script type="text/javascript">
   $('#keyboard') .keyboard({
      layout : 'num',  
      usePreview: false,
   });
</script>

<script type="text/javascript">
 $("#myModal").change(function () {
      if ($(this).val() == "2") {
          var options = {
              "backdrop": "show"
          }
          $("#modal-1").modal(options);
      }
  });
</script>

<script type="text/javascript">
$(function () {
    $('#stat-graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'CATI-09'
        },
        subtitle: {
            text: 'Daily Performance'
        },

        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: 'Days'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Respondent'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Respondent: <b>{point.y:.1f} Person</b>'
        },
        series: [{
            name: 'Daily',
            color: '#DF7927',
            data: [
                ['1', 3],
                ['2', 4],
                ['3', 7],
                ['4', 2],
                ['5', 8],
                ['6', 10],
                ['7', 15],
                ['8', 6],
                ['9', 4],
                ['10', 7],
                ['11', 6],
                ['12', 8],
                ['13', 9],
                ['14', 5],
                ['15', 12],
                ['16', 13],
                ['17', 15],
                ['18', 12],
                ['19', 7],
                ['20', 4],
                ['21', 6],
                ['22', 7],
                ['23', 8],
                ['24', 5],
                ['25', 10],
                ['26', 12],
                ['27', 12],
                ['28', 10],
                ['29', 5],
                ['30', 8],
                ['31', 5]
            ],
            dataLabels: {
                color: '#FFFFFF',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
    
    $('#stat-graph-2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'CATI-09'
        },
        subtitle: {
            text: 'Monthly Performance'
        },
        
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: 'Months'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Respondent'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Respondent: <b>{point.y:.1f} Person</b>'
        },
        series: [{
            name: 'Monthly',
            color: '#DF7927',
            data: [
                ['1', 3],
                ['2', 4],
                ['3', 7],
                ['4', 2],
                ['5', 8],
                ['6', 10],
                ['7', 15],
                ['8', 6],
                ['9', 4],
                ['10', 7],
                ['11', 6],
                ['12', 8]
            ],


            dataLabels: {
                color: '#FFFFFF',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('#stat-graph-3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Daily Performance'
        },

        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: 'Days'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Respondent'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Respondent: <b>{point.y:.1f} Person</b>'
        },
        series: [{
            name: 'Daily',
            color: '#DF7927',
            data: [
                ['1', 3],
                ['2', 4],
                ['3', 7],
                ['4', 2],
                ['5', 8],
                ['6', 10],
                ['7', 15],
                ['8', 6],
                ['9', 4],
                ['10', 7],
                ['11', 6],
                ['12', 8],
                ['13', 9],
                ['14', 5],
                ['15', 12],
                ['16', 13],
                ['17', 15],
                ['18', 12],
                ['19', 7],
                ['20', 4],
                ['21', 6],
                ['22', 7],
                ['23', 8],
                ['24', 5],
                ['25', 10],
                ['26', 12],
                ['27', 12],
                ['28', 10],
                ['29', 5],
                ['30', 8],
                ['31', 5]
            ],
            dataLabels: {
                color: '#FFFFFF',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('#stat-graph-4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Performance'
        },
        
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: 'Months'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Respondent'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Respondent: <b>{point.y:.1f} Person</b>'
        },
        series: [{
            name: 'Monthly',
            color: '#DF7927',
            data: [
                ['1', 3],
                ['2', 4],
                ['3', 7],
                ['4', 2],
                ['5', 8],
                ['6', 10],
                ['7', 15],
                ['8', 6],
                ['9', 4],
                ['10', 7],
                ['11', 6],
                ['12', 8]
            ],


            dataLabels: {
                color: '#FFFFFF',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>

<script type="text/javascript">

$(".owl-carousel6-brands").owlCarousel({
    pagination: true,
    navigation: true,
    items: 2,
    addClassActive: true,
    itemsCustom : [
        [0, 1],
        [320, 1]
    ],
    autoPlay : true,
    stopOnHover : true,
});

</script>


<script type="text/javascript">
$(function () {

    $(document).ready(function () {

        // Build the chart
        $('#pie-1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Call Status'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Call Status',
                data: [
                    ['Reschedule', 3],
                    ['Menolak Wawancara', 2],
                    {
                        name: 'Nomor Tidak Terpasang',
                        y: 2,
                        sliced: true,
                        selected: true
                    },
                    ['Tidak Aktif',   2],
                    ['Sukses Interview',    1],
                    ['Tidak Lolos Screening',  2]
                ]
            }]
        });

        // Build the chart
        $('#pie-2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Call Status'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Call Status',
                data: [
                    ['Reschedule', 3],
                    ['Menolak Wawancara', 2],
                    {
                        name: 'Nomor Tidak Terpasang',
                        y: 2,
                        sliced: true,
                        selected: true
                    },
                    ['Tidak Aktif',   2],
                    ['Sukses Interview',    1],
                    ['Tidak Lolos Screening',  2]
                ]
            }]
        });

        // Build the chart
        $('#pie-3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Call Status'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Call Status',
                data: [
                    ['Reschedule', 3],
                    ['Menolak Wawancara', 2],
                    {
                        name: 'Nomor Tidak Terpasang',
                        y: 2,
                        sliced: true,
                        selected: true
                    },
                    ['Tidak Aktif',   2],
                    ['Sukses Interview',    1],
                    ['Tidak Lolos Screening',  2]
                ]
            }]
        });
        
        // Build the chart
        $('#pie-4').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Phone Accuracy'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Phone Accuracy',
                data: [
                    ['Not Accurate', 3],
                    ['Accurate', 2]
                ]
            }]
        });
    });

});
</script>

<script type="text/javascript">
    $(function() {
        $('#reportselector').change(function(){
            $('.report').hide();
            $('#' + $(this).val()).show();
        });

        $('#respondentselector').change(function(){
            $('.respondent').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#addRules").click(function () {   
            var id = ($('.row-rules .control-group .form-group .control-label .radio-inline').length + 1).toString();
            $('.row-rules').append('<div class="row control-group"><div class="col-sm-12"><div class="form-group"><label class="control-label">Rules</label><label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> AND</label><label class="radio-inline"><input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> OR</label></div></div><div class="col-sm-6"><div class="form-group"><label>Field On</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span><select class="form-control select2_sample1"><option value="">--Choose--</option><option value="id">id</option><option value="id_user">id_user</option><option value="id_rules_group">id_rules_group</option><option value="token">token</option><option value="survey_id">survey_id</option><option value="name">name</option><option value="lastname">lastname</option><option value="city">city</option><option value="address">address</option><option value="phone1">phone1</option><option value="phone2">phone2</option><option value="phone3">phone3</option><option value="info_1">info_1</option><option value="info_2">info_2</option><option value="info_3">info_3</option><option value="info_4">info_4</option><option value="info_5">info_5</option><option value="time_zone">time_zone</option><option value="id_call_status">id_call_status</option><option value="dial_event">dial_event</option><option value="sum_intercept">sum_intercept</option><option value="status_qc">status_qc</option><option value="description">description</option><option value="call_by">call_by</option><option value="input_time">input_time</option><option value="update_time">update_time</option><option value="presence">presence</option><option value="random_code">random_code</option></select></div></div></div><div class="col-sm-6"><div class="form-group"><label>On</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span><select class="form-control select2_sample1"><option class="option-field" value="0" selected="selected">- All -</option><option class="option-field" value="1">1</option><option class="option-field" value="1">2</option><option class="option-field" value="1">3</option></select></div></div></div></div>');
        });

        $("#removeRules").click(function () {
            $(".row-rules .control-group:last").remove();
        });
    });
</script> 

<script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
</script> 


<script type="text/javascript">
    $(function(){
        // Instantiate MixItUp:
        $('#filterCaller').mixItUp();      
    });
</script>  

<script type="text/javascript">
    $('.marquee').marquee({
      pauseOnHover: true,
      direction: 'left',
      duration: 8000
    });
</script>















