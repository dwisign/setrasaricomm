<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-auto-scroll="true" data-slide-speed="200">
			<li class="start active ">
				<a href="index.php">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				<span class="selected"></span>
				</a>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-fax"></i>
				<span class="title">Dial</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="dial.php">
						<i class="fa fa-phone"></i>
						Dial</a>
					</li>
					<li>
						<a href="appointment-list.php">
						<i class="fa fa-list-ul "></i>
						Appointment List</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="call-history.php">
				<i class="fa fa-history"></i>
				<span class="title">Call History</span>
				</a>
			</li>
			<li>
				<a href="#" data-toggle="modal" data-target="#myModal-2">
				<i class="fa fa-folder-open"></i>
				<span class="title">Complain Handling</span>
				</a>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END SIDEBAR -->