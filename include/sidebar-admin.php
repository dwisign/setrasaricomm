<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-auto-scroll="true" data-slide-speed="200">
			<li class="start active ">
				<a href="admin.php">
				<i class="fa fa-dashboard"></i>
				<span class="title">Dashboard</span>
				<span class="selected"></span>
				</a>
			</li>
			<li>
				<a href="report.php">
				<i class="fa fa-files-o"></i>
				<span class="title">Report</span>
				</a>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-file-text"></i>
				<span class="title">Project</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-3">
						<i class="fa fa-plus-circle"></i>
						Add Project</a>
					</li>
					<li>
						<a href="project-list.php">
						<i class="fa fa-list-ul "></i>
						Project List</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-users"></i>
				<span class="title">Respondent</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="search-respondent.php">
						<i class="fa fa-search"></i>
						Search Respondent</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-4">
						<i class="fa fa-plus-circle"></i>
						Add Respondent</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-5">
						<i class="fa fa-download"></i>
						Import Respondent</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-sitemap"></i>
				<span class="title">Rules</span>
				<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="add-rules.php">Add Rules
						<i class="fa fa-plus-circle"></i>
						</a>
					</li>
					<li>
						<a href="rules-list.php">Rules List
						<i class="fa fa-list-ul "></i>
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-user"></i>
				<span class="title">Users</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-7">
						<i class="fa fa-plus-circle"></i>
						Add Users</a>
					</li>
					<li>
						<a href="user-list.php">
						<i class="fa fa-list-ul "></i>
						Users List</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-clock-o"></i>
				<span class="title">Appointment</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-8">
						<i class="fa fa-plus-circle"></i>
						Add Appointment</a>
					</li>
					<li>
						<a href="appointment-list-admin.php">
						<i class="fa fa-list-ul "></i>
						Appointment List</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="whisper.php">
				<i class="fa fa-comments"></i>
				<span class="title">Whisper Monitoring</span>
				</a>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-newspaper-o"></i>
				<span class="title">News</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="add-news.php">
						<i class="fa fa-plus-circle"></i>
						Add News</a>
					</li>
					<li>
						<a href="news-list.php">
						<i class="fa fa-list-ul "></i>
						News List</a>
					</li>
				</ul>
			</li>
		<!-- <li>
				<a href="call-history.php">
				<i class="fa fa-history"></i>
				<span class="title">Call History</span>
				</a>
			</li>
			<li>
				<a href="#" data-toggle="modal" data-target="#myModal-2">
				<i class="fa fa-folder-open"></i>
				<span class="title">Complain Handling</span>
				</a>
			</li> -->
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END SIDEBAR -->