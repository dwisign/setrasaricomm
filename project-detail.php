<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Project Detail </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Project Detail</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Progress</span>
									<span class="caption-helper">Fieldwork</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div data-always-visible="1" data-rail-visible1="1">
										<legend>WOW BRAND 2015 - CHM</legend>
				              	  		<div class="row">
					              	  		<div class="col-sm-6">
					              	  		<h4>All Achievement</h4>
					              	  		<ul class="list-group">
												<li class="list-group-item">
													 Total Respondent <span class="badge badge-warning">11</span>
												</li>
												<li class="list-group-item">
													 Called <span class="badge badge-info">06</span>
												</li>
												<li class="list-group-item">
													 Reschedule <span class="badge badge-default">03</span>
												</li>
												<li class="list-group-item">
													 Failed <span class="badge badge-danger">02</span>
												</li>
												<li class="list-group-item">
													 Success <span class="badge badge-success">01</span>
												</li>
											</ul>
											</div>

											<div class="col-sm-6">
					              	  		<h4>Today Target</h4>
					              	  		<ul class="list-group">
												<li class="list-group-item">
													 Target Respondent Per Days <span class="badge badge-warning">10</span>
												</li>
												<li class="list-group-item">
													 Called <span class="badge badge-info">00</span>
												</li>
												<li class="list-group-item">
													 Reschedule <span class="badge badge-default">00</span>
												</li>
												<li class="list-group-item">
													 Failed <span class="badge badge-danger">00</span>
												</li>
												<li class="list-group-item">
													 Success <span class="badge badge-success">00</span>
												</li>
											</ul>
											</div>
										</div>

										</br></br>
										<div class="row">
					              	  		<div class="col-sm-6">
					              	  			<div id="pie-1" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
											</div>

											<div class="col-sm-6">
												<div id="pie-4" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
											</div>
										</div>

										</br></br>
										<div class="row">
					              	  		<div class="col-sm-6">
					              	  			<div id="stat-graph-3" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
											</div>

											<div class="col-sm-6">
												<div id="stat-graph-4" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>