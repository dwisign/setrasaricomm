<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Add Rules</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Add Rules</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">	
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Add</span>
									<span class="caption-helper">Rules</span>
								</div>	
							</div>
							

							<div class="portlet-body">
								<div class="row">
									<div class="col-sm-12">
										<div class="portlet light respondent" id="1">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-users"></i>
													<span class="caption-subject bold uppercase"> Markplus Panel</span>
													<span class="caption-helper">V7 Batch 2</span>
												</div>
											</div>
											<div class="portlet-body">
												<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label>Quota <span class="small text-warning">(required)</span></label>
															<div class="input-group">
															<span class="input-group-addon">
															<i class="fa fa-pie-chart"></i>
															</span>
															<input type="text" class="form-control" placeholder="Quota">
															</div>
														</div>
														</br>
														<div class="row">
															<div class="col-md-6"><button id="addRules" type="button" class="btn btn-default btn-block">Add Rules</button></div>
															<div class="col-md-6"><button id="removeRules" type="button" class="btn btn-danger btn-block">Delete Rules</button></div>
														</div>
													</div>
													<div class="col-md-8">
														<div class="row-rules">
															<div class="row control-group">
																<div class="col-sm-12">
																	<div class="form-group">
																		<label class="control-label">Rules 1</label>
																		<label class="radio-inline">
																		  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> AND
																		</label>
																		<label class="radio-inline">
																		  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> OR
																		</label>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label>Field On</label>
																		<div class="input-group">
																		<span class="input-group-addon">
																		<i class="fa fa-user"></i>
																		</span>
													                    <select class="form-control select2_sample1">
																			<option value="">--Choose--</option>
																			<option value="id">id</option>
																			<option value="id_user">id_user</option>
																			<option value="id_rules_group">id_rules_group</option>
																			<option value="token">token</option>
																			<option value="survey_id">survey_id</option>
																			<option value="name">name</option>
																			<option value="lastname">lastname</option>
																			<option value="city">city</option>
																			<option value="address">address</option>
																			<option value="phone1">phone1</option>
																			<option value="phone2">phone2</option>
																			<option value="phone3">phone3</option>
																			<option value="info_1">info_1</option>
																			<option value="info_2">info_2</option>
																			<option value="info_3">info_3</option>
																			<option value="info_4">info_4</option>
																			<option value="info_5">info_5</option>
																			<option value="time_zone">time_zone</option>
																			<option value="id_call_status">id_call_status</option>
																			<option value="dial_event">dial_event</option>
																			<option value="sum_intercept">sum_intercept</option>
																			<option value="status_qc">status_qc</option>
																			<option value="description">description</option>
																			<option value="call_by">call_by</option>
																			<option value="input_time">input_time</option>
																			<option value="update_time">update_time</option>
																			<option value="presence">presence</option>
																			<option value="random_code">random_code</option>		
																		</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label>On</label>
																		<div class="input-group">
																		<span class="input-group-addon">
																		<i class="fa fa-user"></i>
																		</span>
																		<select class="form-control select2_sample1">
												                            <option class="option-field" value="0" selected="selected">- All -</option>
												                            <option class="option-field" value="1">1</option>
												                            <option class="option-field" value="1">2</option>
												                            <option class="option-field" value="1">3</option>
												                        </select>
																		</div>
																	</div>
																</div>
																<hr>
															</div>
														</div>

														</br>	
														<div class="form-group">
															<a href="rules-list.php" type="button" class="btn btn-success btn-block">Save</a>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
					

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>