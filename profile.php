<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="pull-right">
					<button type="button" class="btn btn-circle yellow"><b>Project :</b> WOW BRAND 2015 - CHM</button>
				</div>
				<h3 class="page-title">
				<b>Setrasaricomm</b> | User Profile </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.php">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Profile</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				

				<div class="clearfix">
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">User</span>
									<span class="caption-helper">Profile</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									
										<div class="row">
											<div class="col-sm-3">
												<div class="form-group">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div class="fileinput-new thumbnail">
															<img src="img/no-image.jpg" alt=""/>
														</div>
														<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 155px; max-height: 155px;">
														</div>
														<div>
															<span class="btn default btn-file">
															<span class="fileinput-new">
															Select image </span>
															<span class="fileinput-exists">
															Change </span>
															<input type="file" name="...">
															</span>
															<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
															Remove </a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-9">
												<form class="form-horizontal sweet-form" role="form">
													<div class="form-group">
														<label class="control-label col-md-3">User Name</label>
														<div class="col-md-9">
															<p class="form-control-static">
																  CATI-09
															</p>
														</div>
													</div>
													<hr>
													<div class="form-group">
														<label class="control-label col-md-3">Name</label>
														<div class="col-md-9">
															<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-user"></i>
																</span>
																<input type="text" class="form-control" value="CATI-09">
															</div>
														</div>
													</div>
													<hr>
													<div class="form-group">
														<label class="control-label col-md-3">Password</label>
														<div class="col-md-9">
															<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-lock"></i>
																</span>
																<input type="password" class="form-control" value="12345678">
															</div>
														</div>
													</div>
													<hr>
													</br>
													<div class="row">
														<div class="col-sm-3">
															<button class="btn btn-success btn-block">Save</button>
														</div>
													</div>
												</form>
												
											</div>
										</div>

										

									
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>