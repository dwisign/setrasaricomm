<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | News List </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">News List</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">News</span>
									<span class="caption-helper">List</span>
								</div>
								<div class="actions">
									<a href="add-news.php" class="btn btn-circle red-sunglo">
									<i class="fa fa-plus-circle"></i> Add News</a>	
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div data-always-visible="1" data-rail-visible1="1">
										<table class="table table-striped table-bordered table-hover" id="sample_3">
										<thead>
										<tr>
											<th>No</th>
											<th>Title</th>
											<th>Content Preview</th>
											<th>Date Input</th>
											<th>Status</th>
											<th>Modify</th>
										</tr>
										</thead>
										<tbody>
										<tr class="odd gradeX">
											<td>1.</td>
											<td>Lorem Ipsum Dolor Sit Amet</td>
											<td width="500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</td>
											<td>18-05-2015 | 20:05:00</td>	
											<td>Published</td>	
											<td class="text-center">
												<a href="add-news-edit.php" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
												<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
											</td>	
										</tr>
										<tr class="odd gradeX">
											<td>2.</td>
											<td>Lorem Ipsum Dolor Sit Amet</td>
											<td width="500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</td>
											<td>18-05-2015 | 20:05:00</td>	
											<td>Published</td>	
											<td class="text-center">
												<a href="add-news-edit.php" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
												<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
											</td>	
										</tr>
										<tr class="odd gradeX">
											<td>3.</td>
											<td>Lorem Ipsum Dolor Sit Amet</td>
											<td width="500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</td>
											<td>18-05-2015 | 20:05:00</td>	
											<td>Published</td>	
											<td class="text-center">
												<a href="add-news-edit.php" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
												<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
											</td>	
										</tr>
										<tr class="odd gradeX">
											<td>4.</td>
											<td>Lorem Ipsum Dolor Sit Amet</td>
											<td width="500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</td>
											<td>18-05-2015 | 20:05:00</td>	
											<td>Published</td>	
											<td class="text-center">
												<a href="add-news-edit.php" style="color: #666;" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp
												<a href="#" style="color: #666;" data-toggle="tooltip" data-placement="top" title="delete"><i class="fa fa-trash fa-lg"></i></i></a>
											</td>	
										</tr>
										
										
										</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						
					</div>
				</div>
				

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>