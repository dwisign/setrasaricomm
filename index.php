<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="pull-right">
					<button type="button" class="btn btn-circle yellow"><b>Project :</b> WOW BRAND 2015 - CHM</button>
				</div>
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Dashboard </h3>
				<div class="page-bar">
					<div class="row">
						<div class="col-xs-4">
							<ul class="page-breadcrumb">
								<li>
									<i class="fa fa-home"></i>
									<a href="index.php">Home</a>
									<i class="fa fa-angle-right"></i>
								</li>
								<li>
									<a href="#">Dashboard</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-7">
							<div class="marquee">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
						</div>
						<div class="col-xs-1">
							<div class="page-toolbar">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
									NEWS
									</button>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN DASHBOARD STATS -->
				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-bar-chart font-green-sharp hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">OVERALL</span>
									<span class="caption-helper">Progress</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="row">
									<div class="col-lg-4 col-xs-12">
										<a class="dashboard-stat dashboard-stat-light yellow-casablanca" href="#">
										<div class="visual">
											<i class="fa fa-users"></i>
										</div>
										<div class="details">
											<div class="number">
												 6
											</div>
											<div class="desc">
												 Respondent
											</div>
										</div>
										</a>
									</div>
									<div class="col-lg-4 col-xs-12">
										<a class="dashboard-stat dashboard-stat-light red-pink" href="#">
										<div class="visual">
											<i class="fa fa-times-circle"></i>
										</div>
										<div class="details">
											<div class="number">
												 5
											</div>
											<div class="desc">
												 Failed
											</div>
										</div>
										</a>
									</div>
									<div class="col-lg-4 col-xs-12">
										<a class="dashboard-stat dashboard-stat-light green-haze" href="#">
										<div class="visual">
											<i class="fa fa-check-circle"></i>
										</div>
										<div class="details">
											<div class="number">
												 1
											</div>
											<div class="desc">
												 Success
											</div>
										</div>
										</a>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-bar-chart font-green-sharp hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">TODAY</span>
									<span class="caption-helper">Progress</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="row">
									<div class="col-lg-4 col-xs-12">
										<a class="dashboard-stat dashboard-stat-light yellow-casablanca" href="#">
										<div class="visual">
											<i class="fa fa-users"></i>
										</div>
										<div class="details">
											<div class="number">
												 10
											</div>
											<div class="desc">
												 Target Respondent
											</div>
										</div>
										</a>
									</div>
									<div class="col-lg-4 col-xs-12">
										<a class="dashboard-stat dashboard-stat-light red-pink" href="#">
										<div class="visual">
											<i class="fa fa-times-circle"></i>
										</div>
										<div class="details">
											<div class="number">
												 0
											</div>
											<div class="desc">
												 Failed
											</div>
										</div>
										</a>
									</div>
									<div class="col-lg-4 col-xs-12">
										<a class="dashboard-stat dashboard-stat-light green-haze" href="#">
										<div class="visual">
											<i class="fa fa-check-circle"></i>
										</div>
										<div class="details">
											<div class="number">
												 0
											</div>
											<div class="desc">
												 Success
											</div>
										</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-lg-7">
						<!-- BEGIN PORTLET-->
						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-bar-chart font-green-sharp hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Daily</span>
									<span class="caption-helper">Performances</span>
								</div>
							</div>
							<div class="portlet-body">
								<div id="stat-graph" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>

					<div class="col-lg-5">
						<!-- BEGIN PORTLET-->
						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-bar-chart font-green-sharp hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Monthly</span>
									<span class="caption-helper">Performances</span>
								</div>
							</div>
							<div class="portlet-body">
								<div id="stat-graph-2" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>
				</div>
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Project</span>
									<span class="caption-helper">Description</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
										<h3>WOW BRAND 2015 - CHM</h3>
										<p>Ini adalah survei sindikasi wow brand champion pada katagori CHM</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="clearfix">
				</div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>