<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-auto-scroll="true" data-slide-speed="200">
					<li>
						<a href="index.php">
						<i class="icon-home"></i>
						<span class="title">Dashboard</span>
						</a>
					</li>
					<li>
						<a href="javascript:;">
						<i class="fa fa-fax"></i>
						<span class="title">Dial</span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="dial.php">
								<i class="fa fa-phone"></i>
								Dial</a>
							</li>
							<li>
								<a href="appointment-list.php">
								<i class="fa fa-list-ul "></i>
								Appointment List</a>
							</li>
						</ul>
					</li>
					<li class="start active ">
						<a href="call-history.php">
						<i class="fa fa-history"></i>
						<span class="title">Call History</span>
						<span class="selected"></span>
						</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal-2">
						<i class="fa fa-folder-open"></i>
						<span class="title">Complain Handling</span>
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="pull-right">
					<button type="button" class="btn btn-circle yellow"><b>Project :</b> WOW BRAND 2015 - CHM</button>
				</div>
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Call History </h3>
				<div class="page-bar">
					<div class="row">
						<div class="col-xs-4">
							<ul class="page-breadcrumb">
								<li>
									<i class="fa fa-home"></i>
									<a href="index.php">Home</a>
									<i class="fa fa-angle-right"></i>
								</li>
								<li>
									<a href="#">Call History</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-7">
							<div class="marquee"><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</a></div>
						</div>
						<div class="col-xs-1">
							<div class="page-toolbar">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
									NEWS
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				

				<div class="clearfix">
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Report</span>
									<span class="caption-helper">Export Call History</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
										<div class="filter">
										<!-- BEGIN FILTER-->
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<label>Name</label>
														<div class="input-group">
															<span class="input-group-addon">
															<i class="fa fa-user"></i>
															</span>
															<input type="text" class="form-control" placeholder="Name">
														</div>
													</div>

													<div class="form-group">
														<label>Token</label>
														<div class="input-group">
															<span class="input-group-addon">
															<i class="fa fa-lock"></i>
															</span>
															<input type="text" class="form-control" placeholder="Token">
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label>Call Status</label>
														<div class="input-group">
														<span class="input-group-addon">
														<i class="fa fa-phone"></i>
														</span>
														<select class="form-control select2_sample1">
															<option value="fluid" selected="selected">- Choose -</option>
															<option value="boxed">Sukses Interview</option>
															<option value="boxed">Reschedule/Jadwal Ulang</option>
															<option value="boxed">Menolak wawancara</option>
															<option value="boxed">Tidak Lolos Screening</option>
															<option value="boxed">Salah Sambung</option>
															<option value="boxed">Nomor Tidak Terpasang</option>
															<option value="boxed">Tidak Ada Nada</option>
															<option value="boxed">Tidak Ada Jawaban</option>
															<option value="boxed">Tidak Aktif</option>
														</select>
														</div>
													</div>
													<div class="form-group">
														<label>Status QC</label>
														<div class="input-group">
														<span class="input-group-addon">
														<i class="fa fa-cogs"></i>
														</span>
														<select class="form-control select2_sample1">
															<option value="fluid" selected="selected">- Choose -</option>
															<option value="boxed">Pending</option>
															<option value="boxed">Ok</option>
															<option value="boxed">DO</option>
															<option value="boxed">Cadangan</option>
														</select>
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<label class="control-label">Date Range</label>
														<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
															<input type="text" class="form-control" name="from">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="to">
														</div>
														<!-- /input-group -->
														<span class="help-block">Select date range</span>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group filter-search">
														<button class="btn btn-success btn-block">Search</button>
													</div>
												</div>
											</div>
										</div>
										<!-- END BEGIN FILTER-->

										</br></br>

										<!-- BEGIN EXAMPLE TABLE PORTLET-->
										<table class="table table-striped table-bordered table-hover" id="sample_3">
										<thead>
										<tr>
											<th>No.</th>
											<th>Respondent Name</th>
											<th>Token</th>
											<th>City</th>
											<th>Call Status</th>
											<th>Call Time</th>
											<th>Status QC</th>
										</tr>
										</thead>
										<tbody>
										<tr class="odd gradeX">
											<td>1.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Success Interview
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>2.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Reschedule/Jadwal Ulang
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>3.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Menolak wawancara
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>4.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Success Interview
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>5.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Success Interview
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>6.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Reschedule/Jadwal Ulang
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>7.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Success Interview
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>8.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Menolak wawancara
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>

										<tr class="odd gradeX">
											<td>9.</td>
											<td>Ir. Tri Rismaharini, MT</td>
											<td>CATJKTPELANGIR50</td>
											<td>Jakarta</td>	
											<td>
												Menolak wawancara
											</td>
											<td>2015-01-07 11:56:35</td>	
											<td>
												Pending
											</td>
										</tr>
										</tbody>
										</table>
										</br>
										<div class="row">
											<div class="col-sm-3 pull-right">
												<button class="btn btn-danger btn-block">Export</button>
											</div>
										</div>

										<!-- END EXAMPLE TABLE PORTLET-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="clearfix">
				</div>

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>