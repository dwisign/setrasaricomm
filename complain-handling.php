<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">COMPLAIN HANDLING</h4>
      </div>
      <div class="modal-body">
            <div class="alert alert-success alert-dismissable collapse" id="collapseExample">
              Well done and everything looks OK. <a href="" class="alert-link">
              Please check this one as well </a>
            </div>

            <div class="form-group">
              <label>Title</label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-shield"></i>
              </span>
              <input class="form-control form-control-inline" size="16" type="text" value=""/>
              </div>
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control" rows="3"></textarea>
            </div>

            <div class="form-group">
              <label>Pilih Kota</label>
              <div class="input-group">
              <span class="input-group-addon">
              <i class="fa fa-map-marker"></i>
              </span>
              <select class="form-control select2_sample1" data-placeholder="Pilih Kota" >
                <option value="1" selected="selected">Jakarta</option>
                <option value="2">Bandung</option>
                <option value="3">Semarang</option>
                <option value="4">Medan</option>
                <option value="5">Mokhtar</option>
                <option value="6">Denpasar</option>
                <option value="7">Yogyakarta</option>
                <option value="8">Surabaya</option>
              </select>
              </div>
            </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="collapse" href="#collapseExample">Submit</button>
      </div>
    </div>
  </div>
</div>