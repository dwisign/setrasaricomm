<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
    require("include/source.php");
    ?> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
	<?php
    require("include/header-admin.php");
    ?> 
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="container-fluid">
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
			<?php
		    require("include/sidebar-admin.php");
		    ?> 
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<b>Setrasaricomm</b> | Report</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>
							<a href="admin.php">Admin Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Report</a>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<div class="clearfix">
				</div>
				

				<div class="row">
					<div class="col-sm-12">
						<div class="portlet light tasks-widget">
							<div class="portlet-title">	
								<div class="caption">
									<i class="icon-share font-green-haze hide"></i>
									<span class="caption-subject font-yellow-casablanca bold uppercase">Project</span>
									<span class="caption-helper">Report</span>
								</div>	
							</div>
							
							<div class="form-group well">
								<label>Kind Of Report</label>
								<div class="input-group">
								<span class="input-group-addon">
								<i class="fa fa-file-text"></i>
								</span>
								<select class="form-control select2_sample1" data-placeholder="User Category" id="reportselector">
									<option value="callhistory">Call History</option>
									<option value="workhistory">Work History</option>
									<option value="callstatus">Call Status</option>
									<option value="rekapcati">Rekap CATI</option>
									<option value="catiaccuracy">Hourly CATI Accuracy</option>
								</select>
								</div>
							</div>


							<div class="portlet-body">
								<div class="row">
									<div class="col-sm-12">
										<div class="portlet light report" id="callhistory">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-phone"></i>
													<span class="caption-subject bold uppercase"> CALL</span>
													<span class="caption-helper">History</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label>Project</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Project">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Simbal</option>
																	<option value="2">WOW Brand 2015 - CHM</option>
																	<option value="3">Auto Delloite - New</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label>User</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-users"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="User">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Name</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-user"></i>
																</span>
																<input type="text" class="form-control" placeholder="Name">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>Dial Number</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-phone"></i>
																</span>
																<input type="text" class="form-control" placeholder="Dial Number">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label class="control-label">Date Range</label>
																<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
																	<input type="text" class="form-control" name="from">
																	<span class="input-group-addon">to</span>
																	<input type="text" class="form-control" name="to">
																</div>
																<!-- /input-group -->
																<span class="help-block">Select date range</span>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>

													<table class="table table-striped table-bordered table-hover" id="sample_3">
													<thead>
													<tr>
														<th>No.</th>
														<th>Project</th>
														<th>Respondent Name</th>
														<th>Token</th>
														<th>City</th>
														<th>Dial Number</th>
														<th>Call By</th>
														<th>Call Time</th>
														<th>Accuracy</th>
														<th>Duration</th>
														<th>Recording</th>
													</tr>
													</thead>
													<tbody>
													<tr class="odd gradeX">
														<td>1.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>2.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>3.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>4.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>5.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>6.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>7.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>8.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>9.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													<tr class="odd gradeX">
														<td>10.</td>
														<td>WOW Brand 2015-CHM</td>
														<td>Bima Arya</td>
														<td>07634638209374</td>	
														<td>Jakarta</td>	
														<td>021234567</td>	
														<td>Naoki Uehara</td>
														<td>15.30</td>
														<td>-</td>
														<td>00:30:20</td>
														<td>-</td>
													</tr>
													</tbody>
													</table>

											</div>
										</div>

										<div class="portlet light report" id="workhistory" style="display:none">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-pencil-square-o"></i>
													<span class="caption-subject bold uppercase"> WORK</span>
													<span class="caption-helper">History</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label>Project</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Project">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Simbal</option>
																	<option value="2">WOW Brand 2015 - CHM</option>
																	<option value="3">Auto Delloite - New</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>User</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-users"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose User">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label class="control-label">Date Range</label>
																<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
																	<input type="text" class="form-control" name="from">
																	<span class="input-group-addon">to</span>
																	<input type="text" class="form-control" name="to">
																</div>
																<!-- /input-group -->
																<span class="help-block">Select date range</span>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>

														<table class="table table-striped table-bordered table-hover" id="sample_4">
														<thead>
														<tr>
															<th>No.</th>
															<th>Project</th>
															<th>Respondent Name</th>
															<th>Token</th>
															<th>Info_1</th>
															<th>Info_2</th>
															<th>Dial Number</th>
															<th>Call History</th>
															<th>Call By</th>
															<th>Call Time</th>
															<th>Duration</th>
														</tr>
														</thead>
														<tbody>
														<tr class="odd gradeX">
															<td>1.</td>
															<td>WOW Brand 2015-CHM</td>
															<td>Bima Arya</td>
															<td>07634638209374</td>	
															<td>-</td>	
															<td>-</td>	
															<td>021234567</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
														</tr>
														</tbody>
														</table>

											</div>
										</div>

										<div class="portlet light report" id="callstatus" style="display:none">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-phone"></i>
													<span class="caption-subject bold uppercase"> CALL</span>
													<span class="caption-helper">Status</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label>Project</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Project">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Simbal</option>
																	<option value="2">WOW Brand 2015 - CHM</option>
																	<option value="3">Auto Delloite - New</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label>User</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-users"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose User">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">CATI - 09</option>
																	<option value="2">CATI - 12</option>
																	<option value="3">CATI - 10</option>
																</select>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label class="control-label">Date Range</label>
																<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
																	<input type="text" class="form-control" name="from">
																	<span class="input-group-addon">to</span>
																	<input type="text" class="form-control" name="to">
																</div>
																<!-- /input-group -->
																<span class="help-block">Select date range</span>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label>Call Status</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-users"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Call Status">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Sukses Interview</option>
																	<option value="2">Reschedule/Jadwal Ulang</option>
																	<option value="3">Menolak wawancara</option>
																	<option value="#">Tidak Lolos Screening</option>
																	<option value="#">Salah Sambung</option>
																	<option value="#">Nomor Tidak Terpasang</option>
																	<option value="#">Tidak Ada Nada</option>
																	<option value="#">Tidak Ada Jawaban</option>
																	<option value="#">Tidak Aktif</option>
																</select>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>
														<div class="table-responsive">
														<table class="table table-striped table-bordered table-hover" id="sample_5">
														<thead>
														<tr>
															<th>Caller ID</th>
															<th>Token</th>
															<th>Name</th>
															<th>Last Name</th>
															<th>City</th>
															<th>Address</th>
															<th>Phone_1</th>
															<th>Phone_2</th>
															<th>Phone_3</th>
															<th>Info_1</th>
															<th>Info_2</th>
															<th>Info_3</th>
															<th>Info_4</th>
															<th>Info_5</th>
															<th>ID_Call_Status</th>
															<th>Call_Status</th>
															<th>Description</th>
															<th>Total Call</th>
															<th>Call_Time</th>
															<th>Upload_Time</th>
														</tr>
														</thead>
														<tbody>
														<tr class="odd gradeX">
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>	
															<td>-</td>	
															<td>-</td>	
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>	
															<td>-</td>	
															<td>-</td>	
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
														</tr>
														</tbody>
														</table>
														</div>
											</div>
										</div>

										<div class="portlet light report" id="rekapcati" style="display:none">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-clipboard"></i>
													<span class="caption-subject bold uppercase"> REKAP</span>
													<span class="caption-helper">Cati</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label>Project</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Project">
																	<option value="0" selected="selected">- Choose -</option>
																	<option value="1">Simbal</option>
																	<option value="2">WOW Brand 2015 - CHM</option>
																	<option value="3">Auto Delloite - New</option>
																</select>
																</div>
															</div>
														</div>
														
														<div class="col-sm-6">
															<div class="form-group">
																<label class="control-label">Date Range</label>
																<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
																	<input type="text" class="form-control" name="from">
																	<span class="input-group-addon">to</span>
																	<input type="text" class="form-control" name="to">
																</div>
																<!-- /input-group -->
																<span class="help-block">Select date range</span>
															</div>
														</div>
														

														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>

														<table class="table table-striped table-bordered table-hover" id="sample_6">
														<thead>
														<tr>
															<th>ID</th>
															<th>CATI</th>
															<th>Sukses</th>
															<th>Reschedule</th>
															<th>Menolak</th>
															<th>Gagal Screening</th>
															<th>Salah Sambung </th>
															<th>Nomor Tidak Terpasang</th>
															<th>Tidak Ada Nada</th>
															<th>Tidak Ada Jawaban</th>
															<th>Tidak Aktif</th>
														</tr>
														</thead>
														<tbody>
														<tr class="odd gradeX">
															<td>01</td>
															<td>CATI-01</td>
															<td>-</td>
															<td>-</td>	
															<td>-</td>	
															<td>-</td>	
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
														</tr>
														</tbody>
														</table>
											</div>
										</div>

										<div class="portlet light report" id="catiaccuracy" style="display:none">
											<div class="portlet-title">
												<div class="caption font-red-sunglo">
													<i class="fa fa-clock-o"></i>
													<span class="caption-subject bold uppercase"> Hourly</span>
													<span class="caption-helper">Cati Accuracy</span>
												</div>
											</div>
											<div class="portlet-body">
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label>Project</label>
																<div class="input-group">
																<span class="input-group-addon">
																<i class="fa fa-file-text"></i>
																</span>
																<select class="form-control select2_sample1" data-placeholder="Choose Project">
																	<option value="1">Markplus Panel - V7 Batch 2</option>
																	<option value="2">WOW BRand 2015 - CHM</option>
																	<option value="3">Simbal</option>
																	<option value="4">Auto Delloite - New</option>
																</select>
																</div>
															</div>
														</div>
														
														<div class="col-sm-6">
															<div class="form-group">
																<label class="control-label">Date Range</label>
																<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
																	<input type="text" class="form-control" name="from">
																	<span class="input-group-addon">to</span>
																	<input type="text" class="form-control" name="to">
																</div>
																<!-- /input-group -->
																<span class="help-block">Select date range</span>
															</div>
														</div>
														

														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-success btn-block">Search</button>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group filter-search">
																<button class="btn btn-danger btn-block">Export</button>
															</div>
														</div>
													</div>

													</br></br>

														<table class="table table-striped table-bordered table-hover" id="sample_7">
														<thead>
														<tr>
															<th>No.</th>
															<th>Info 1</th>
															<th>Time</th>
															<th>Connected Call</th>
															<th>Success Call</th>
														</tr>
														</thead>
														<tbody>
														<tr class="odd gradeX">
															<td>1.</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>	
															<td>-</td>	
														</tr>
														</tbody>
														</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="clearfix"></div>
				

			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	 <?php 	
	 require("include/footer.php");
	 ?>
	<!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<?php 	
 require("include/js.php");
 ?>
 <!-- END JAVASCRIPTS --> 

</body>
<!-- END BODY -->
</html>